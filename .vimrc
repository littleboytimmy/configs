set nocompatible
filetype off

" Functions
" toggle between number and relativenumber
function! ToggleNumber()
  if(&relativenumber == 1)
    set norelativenumber
    set number
  else
    set relativenumber
  endif
endfunc
 
" check if a colorscheme exists
" {rtp}/autoload/has.vim
function! HasColorscheme(name)
    let pat = 'colors/'.a:name.'.vim'
    return !empty(globpath(&rtp, pat))
endfunction

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" required plugin
Plugin 'VundleVim/Vundle.vim'

" syntax and language plugins
Plugin 'hashivim/vim-terraform'
Plugin 'Chiel92/vim-autoformat'
Plugin 'elzr/vim-json'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-fugitive'
Plugin 'saltstack/salt-vim'
Plugin 'Glench/Vim-Jinja2-Syntax'
Plugin 'luochen1990/rainbow'

" Prettifying
Plugin 'godlygeek/tabular'

" command plugins
Plugin 'jiangmiao/auto-pairs'

" colorscheme plugins
Plugin 'flazz/vim-colorschemes'
Plugin 'morhetz/gruvbox'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'NLKNguyen/papercolor-theme'


" changing plugins
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdtree'

" other plugins
Plugin 'vim-airline/vim-airline'

" airline plugins
Plugin 'enricobacis/vim-airline-clock'

call vundle#end()
filetype plugin indent on

" basic settings for editor
set number
syntax on
set t_Co=256
set background=dark
set tabstop=2
set shiftwidth=2
set expandtab
set cursorline

" see if PaperColor colorscheme exists
if HasColorscheme('PaperColor')
  colorscheme PaperColor
  let g:airline_theme='papercolor'
else
  colorscheme murphy
endif

" plugin settings
let g:Tlist_Ctags_Cmd='/usr/local/Cellar/ctags/5.8_1/bin/ctags'
let g:NERDTreeNodeDelimiter = "\u00a0"
let mapleader=","
let g:rainbow_active = 1
set modeline
filetype plugin indent on
let g:sls_use_jinja_syntax = 1

" start nerdtree if no file open in vim
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" on save
au BufWrite *.tf :Autoformat
au BufWrite *.tf :Tab /=

" Mappings
nmap <F8> :TagbarToggle<CR>
nmap <F7> :NERDTreeToggle<CR>
nmap <F6> :Autoformat<CR>

" Settings for filetype syntax
autocmd BufNewFile,BufRead *.tpl set syntax=bash
